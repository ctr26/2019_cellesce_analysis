# https://www.nature.com/articles/nmeth.4397

# %% Imports
import importlib
importlib.reload(pd)
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.metrics import homogeneity_score
from sklearn.cluster import KMeans
import numpy.matlib
from scipy.stats import kstest
import scipy.stats

sns.set()

# %% Setup
SAVE_FIG = 0
CHANNELS = 2
DRUG = 'G007'
DRUG = 'Dabrafenib'
CONC = 10
# CONC = 3.330

FLAGS_TSNSE = 0
FLAG_TISS = 0
FLAG_MAXCORR = 1
FLAG_MEDIAN_PER_EUCLID = 1
FLAG_CORR_CONC_TO_FEATURE = 1
FLAG_IC50 = 1


ZEROISCONTROL = 1
SAVE_CSV = 1
DROP_TEXT_FROM_DF = 1

# %% Read data

# Load data
#root_dir = './_analysed/2colour_largeset/'
# data_folder = '220519 - G007'
data_folder = '230719'
root_dir = './_analysed/'+data_folder+'/';root_dir
metadata = lambda : "./graphs/"+data_folder+"_channels_"+str(CHANNELS)

# root_dir = './_analysed/190709/'
# root_dir = './_analysed/230719/'
object_file_path = root_dir + 'Nuclei.csv'
image_file_path = root_dir + 'Image.csv'

object_file_df = pd.read_csv(object_file_path)
image_file_df = pd.read_csv(image_file_path)
merged_full_data_set = object_file_df.merge(image_file_df,
                                            on='ImageNumber',how='left');merged_full_data_set.head()
### Fix conc column
pattern = r'ISO34_(?P<Drug>[^_\s]+)[_\s](?P<Concentration>[\w\d]*uM)'
filenames = merged_full_data_set['PathName_Channels'];
extracted_data = filenames.str.extract(pattern)

# extracted_data.

# little= object_file_df[['ObjectNumber','ImageNumber']]
# cells_per_image = little.groupby('ImageNumber').max()
#
# plt.hist(cells_per_image['ObjectNumber'])
# np.median(cells_per_image['ObjectNumber'])
# np.average(cells_per_image['ObjectNumber'])

extracted_data['Conc /uM'] = pd.to_numeric((extracted_data['Concentration'].\
                                str.replace('_','.')).\
                                str.replace('uM','')).round(decimals=3)


if(ZEROISCONTROL):
    extracted_data['Drug'] = extracted_data['Drug'].where(extracted_data['Conc /uM']!=0,other='Control')

merged_full_data_set_extracted = merged_full_data_set\
                .join(extracted_data)\
                .dropna(axis=0);merged_full_data_set_extracted.head()

# merged_full_data_set_extracted.to_csv('checkme.csv')
merged_full_data_set_extracted_indexed =  merged_full_data_set_extracted\
        .set_index(['ObjectNumber','ImageNumber','Drug','Conc /uM'])


# Merge conc and drug name column
# Left merge

df_full_clean = merged_full_data_set_extracted_indexed\
                                                # .apply(pd.to_numeric, errors='coerce')\
                                                # .dropna(axis='columns')\
                                                # .astype(float)\
                                                # .dropna()
# df_full_clean
object_file_df_indexed = object_file_df\
                                .join(extracted_data)\
                                .set_index(['ObjectNumber','ImageNumber','Drug','Conc /uM'])\
                                # .apply(pd.to_numeric, errors='coerce')\
                                # .dropna(axis='columns')\
                                # .astype(float)\
                                # .dropna()

# object_file_df_indexed.to_csv('checkme.csv')


if(CHANNELS==1):
    df = object_file_df_indexed
    # csv_name = data_folder+'_objects'
    csv_name = root_dir+'df_1Channel'
if(CHANNELS==2):
    df = df_full_clean
    # csv_name = root_dir+'df_objects_image'
    csv_name = root_dir+'df_2Channel'

if(DROP_TEXT_FROM_DF):
    df = df.apply(pd.to_numeric, errors='coerce')\
        .dropna(axis='columns')\
        .astype(float)\
        .dropna()

df.unstack('Drug').groupby('ImageNumber').median().stack().groupby('Drug').count()
df_median = df.unstack(level=['Drug','Conc /uM']).\
            groupby(level='ImageNumber').\
            median().\
            stack(level=['Conc /uM','Drug'])
SAVE_CSV=0
# csv_name+'_median_per_org_'+'.csv'
if(SAVE_CSV):
    df.\
    reset_index().\
    dropna(axis=0).\
    to_csv(csv_name+'.csv',index=False)

    df_median.\
    reset_index().\
    dropna(axis=0).\
    to_csv(csv_name+'_median_per_org_'+'.csv',index=False)

index = df.index
scaled_df = pd.DataFrame(StandardScaler().fit_transform(df),index=df.index)


# %% Df summary

df_summary = df_median.groupby(['Drug','Conc /uM']).count().rename(columns={'AreaShape_Area':'Organoids'})
sns.catplot(y='Conc /uM',
            hue='Drug',x='Organoids',
            kind="bar",orient="h",
            data=df_summary['Organoids'].reset_index())
if(SAVE_FIG):plt.savefig(metadata()+'_summary.pdf')
plt.show()

# %%## PCA Analysis ####

# df_kmeans = df.copy()
# kmeans_df = KMeans(n_clusters=4, random_state=0).fit(df)
# df_kmeans['kmeans'] = kmeans_df.labels_
# df_kmeans=df_kmeans.set_index('kmeans',append=True)

df_CONC = df.xs(CONC,level='Conc /uM',drop_level=False)
df_median_CONC = df_median.xs(CONC,level='Conc /uM',drop_level=False)
df_CONC_control = df.xs(CONC,level='Conc /uM',drop_level=False).\
                    append(df.xs('Control',level='Drug',drop_level=False))
df_median_CONC_control = df_median.xs(CONC,level='Conc /uM',drop_level=False).\
                    append(df_median.xs('Control',level='Drug',drop_level=False))

pca_variants = [(df,'per_cell','Conc /uM',None),
                (df,'per_cell_size','Drug','Conc /uM'),
                (df_median,'per_organoid','Conc /uM',None),
                (df_CONC,'per_cell_'+str(CONC)+'uM','Drug',None),
                (df_median_CONC,'per_organoid_'+str(CONC)+'uM','Drug',None),
                (df_CONC_control,'per_cell_'+str(CONC)+'uM_control','Drug',None),
                (df_median_CONC_control,'per_organoid_'+str(CONC)+'uM_control','Drug',None)
                 ]
# pca_variants = [(df,'per_cell_size','Drug','Conc /uM')]
for i in pca_variants:
    pca_df,type,hue,size = i
    # print(i)
    index_pca = pca_df.index
    scaled_df_pca = pd.DataFrame(StandardScaler().fit_transform(pca_df),index=pca_df.index)
    principalComponents = PCA(n_components=2).fit_transform(scaled_df_pca)
    principalDf = pd.DataFrame(data = principalComponents
                 , columns = ['principal component 1', 'principal component 2'],index=pca_df.index)
    if(hue=='Drug'):
        cluster_num = len(scaled_df_pca.reset_index()['Drug'].unique());cluster_num
        scaled_df_pca_kmeans = KMeans(n_clusters=cluster_num, random_state=0).fit(principalComponents)
        kmeans_accuracy = homogeneity_score(scaled_df_pca_kmeans.labels_,scaled_df_pca.reset_index()['Drug'])

        xx,yy = np.meshgrid(np.linspace(principalComponents[:,0].min(),principalComponents[:,0].max(),500),
                            np.linspace(principalComponents[:,1].min(),principalComponents[:,1].max(),500))
        Z = scaled_df_pca_kmeans.predict(np.c_[xx.ravel(), yy.ravel()]).reshape(xx.shape)
        plt.imshow(Z, interpolation='nearest',
                    extent=(xx.min(), xx.max(), yy.min(), yy.max()),
                    cmap='Pastel2',
                    aspect='auto', origin='lower')
        plt.title(f'K means score: {kmeans_accuracy:.2f}');
    sns.scatterplot(x='principal component 1',
                    y="principal component 2",
                    hue=hue,size=size,
                    data=principalDf.reset_index())
    if(SAVE_FIG):plt.savefig(metadata()+"_PCA_"+type+".pdf");plt.show()
    if(not(hue == 'Drug')):
        g = sns.FacetGrid(principalDf.reset_index(),col='Drug',hue=hue)
        g.map(sns.scatterplot,"principal component 1","principal component 2").add_legend()
        if(SAVE_FIG):plt.savefig(metadata()+"_PCA_facet_"+type+".pdf");plt.show()


# %%##### TSNE https://lvdmaaten.github.io/tsne/

if(FLAGS_TSNSE):
    from sklearn.manifold import TSNE

    X_embedded = TSNE(n_components=2).fit_transform(scaled_df)
    # principalComponents = pca.fit_transform(scaled_df)
    X_embeddedDf = pd.DataFrame(data = X_embedded, columns = ['tsne component 1', 'tsne component 2'],index=index)
    # tnse_df_meta = X_embeddedDf.join(merged_full_data_set_extracted[['ImageNumber','Conc /uM','Drug']])

    # sns.scatterplot(x='principal component 1',y="principal component 2",hue="ImageNumber",data=X_embeddedDf)

    sns.scatterplot(x='tsne component 1',y="tsne component 2",hue="Drug",size='Conc /uM',data=X_embeddedDf.reset_index())
    if(SAVE_FIG):plt.savefig(metadata()+"_TSNE.pdf");plt.show()
    g = sns.FacetGrid(X_embeddedDf.reset_index(),col='Drug',hue="Conc /uM")
    g.map(sns.scatterplot,"tsne component 1","tsne component 2").add_legend()
    if(SAVE_FIG):plt.savefig(metadata()+"_TSNE_facet_Drug_Conc.pdf");plt.show()
    g = sns.FacetGrid(X_embeddedDf.reset_index(),col='Conc /uM',hue="Drug")
    g.map(sns.scatterplot,"tsne component 1","tsne component 2").add_legend()
    if(SAVE_FIG):plt.savefig(metadata()+"_TSNE_facet_Conc_Drug.pdf");plt.show()


# %%####### Dimensionality reduction


dimensional_pca_df = pd.DataFrame(\
                PCA(n_components=0.95,whiten=True).fit_transform(scaled_df),\
                index=df.index);dimensional_pca_df.tail()
print("Data dimensionality reduced from " \
        + str(scaled_df.shape[1]) \
        + " to " \
        + str(dimensional_pca_df.shape[1]))

dimensional_pca_df_unstack = dimensional_pca_df.unstack(level='Drug')
dimensional_pca_df_corr = dimensional_pca_df.transpose().corr(method='pearson')
dimensional_pca_df_median = dimensional_pca_df_unstack.groupby(level='Conc /uM').median()

# %% Fingerprints

def df_to_fingerprints(df,median_height=5):
    DRUGS=list(df.index.levels[2]);DRUGS
    plt.rcParams["axes.grid"] = False
    fig, axes = plt.subplots(nrows=8,figsize=(8, 7), dpi=150)
    upper = np.mean(df.values.flatten())+ 3*np.std(df.values.flatten());upper
    lower = np.mean(df.values.flatten())- 3*np.std(df.values.flatten());lower
    for i,ax in enumerate(axes.flat):
        drug = DRUGS[int(np.floor(i/2))];drug
        image = df.xs(drug,level='Drug')
        finger_print = image.median(axis=0)
        finger_print_image = np.matlib.repmat(finger_print.values,median_height,1)

        if i & 1:
            # im = ax.imshow(image, vmin=image.min().min(), vmax=image.max().max(),cmap='Spectral')
            im = ax.imshow(finger_print_image,
                            vmin=lower,
                            vmax=upper,
                            cmap='Spectral',interpolation='nearest')
            ax.set_xticklabels([])
            ax.set_yticklabels([])
        else:
            im = ax.imshow(image,
                            vmin=lower,
                            vmax=upper,
                            cmap='Spectral')
            ax.title.set_text(drug)
            # sns.heatmap(drug_df.values,ax=ax)
            ax.set(adjustable='box',
                    aspect='auto',
                    autoscale_on=False)
            ax.set_xticklabels([])

    fig.subplots_adjust(right=0.8)
    fig.colorbar(im, ax=axes.ravel().tolist())
    # fig.colorbar(im, cax=cbar_ax)
df_to_fingerprints(scaled_df,5)
if(SAVE_FIG):plt.savefig(metadata()+"_df_scaled_dim_colours.pdf")
plt.show()
df_to_fingerprints(dimensional_pca_df,1)
if(SAVE_FIG):plt.savefig(metadata()+"_dimensional_pca_df_colours.pdf")
plt.show()
# %% Correlations between drugs

dimensional_pca_df_median = dimensional_pca_df.groupby(['Conc /uM', 'Drug']).median()
dimensional_pca_df_highconc_drug_corr = dimensional_pca_df_median.xs(10,level='Conc /uM').transpose().corr()

sns.clustermap(dimensional_pca_df_median_drug.transpose().corr(method='pearson'))
# %%  ## Per drug correlations #####

drug_df = dimensional_pca_df.xs(DRUG,level='Drug')
drug_df_corr = drug_df.transpose().corr(method='pearson')

# sns.clustermap(drug_df_corr) # DO NOT RUN
drug_median_per_image = drug_df.unstack(level='Conc /uM').groupby(level='ImageNumber').median().stack()
drug_median_per_conc = drug_df.groupby(level='Conc /uM').median()

drug_median_per_con_corr=drug_median_per_conc.transpose().corr(method='pearson')
drug_median_per_image_corr=drug_median_per_image.transpose().corr(method='pearson')

sns.clustermap(drug_median_per_con_corr);plt.title('Median per conc Pearson')
if(SAVE_FIG):plt.savefig(metadata()+"_"+DRUG+"_median_per_con_corr.pdf")

sns.clustermap(drug_median_per_image_corr);plt.title('Median per Organoid Pearson')
if(SAVE_FIG):plt.savefig(metadata()+"_"+DRUG+"_median_per_image_corr.pdf")

# Groupby does not preserve multindex
dimensional_pca_df_median = dimensional_pca_df.groupby(['ImageNumber','Conc /uM', 'Drug']).median()
# idx_2 = dimensional_pca_df.index.rename(['ObjectNumber II', 'ImageNumber II','Conc /uM II', 'Drug II'])
# dimensional_pca_df_euclid = pd.DataFrame(euclidean_distances(dimensional_pca_df),\
#                             index=dimensional_pca_df.index).T.set_index(idx_2).T
#
#
# dimensional_pca_df_euclid_stacked = pd.DataFrame(\
#                             dimensional_pca_df_euclid.\
#                             stack(['ImageNumber II','Conc /uM II', 'Drug II'])).\
#                             rename(columns={0:'Euclidian distance'})
#
# df_median_euclid_stacked_median_image = df_median_euclid_stacked.\
#                                             groupby(['Conc /uM', 'Drug','Conc /uM II', 'Drug II']).\
#                                             median()
# drug_median_per_image_euclid
# drug_median_per_image_euclid

# drug_median_per_image_euclid_full = df_median_euclid_stacked.xs((DRUG,DRUG),
#                                         level=['Drug','Drug II'],
#                                         drop_level=False)

# drug_median_per_image_euclid
# sns.clustermap(drug_median_per_image_euclid.unstack('Conc /uM II'))

# drug_median_per_image_euclid
# drug_median_per_image_abs_euclid= pd.DataFrame(drug_median_per_image.apply(numpy.linalg.norm,axis=1))
# drug_median_per_image_abs_euclid_df= drug_median_per_image_abs_euclid.rename(columns={0:'y'})
#
# # drug_df_euclid = pd.DataFrame(drug_df.apply(numpy.linalg.norm,axis=1))
# # drug_df_euclid_df = drug_df_eucld.rename(columns={0:'y'})
#
# df_euclid_median = pd.DataFrame(dimensional_pca_df_median.apply(numpy.linalg.norm,axis=1))
# df_euclid_median_df = df_euclid_median.rename(columns={0:'y'})
#
# df_euclid = pd.DataFrame(dimensional_pca_df.apply(numpy.linalg.norm,axis=1))
# df_euclid_df = df_euclid.rename(columns={0:'y'})
#
# grid = sns.lmplot(x='Conc /uM',y='y',
#             col='Drug',data=df_euclid_median_df.reset_index(),sharey=False,x_bins=18,
#             markers=None,hue='Drug')
# grid.set(xscale="log")
# plt.savefig('facet_drugs_dose_median.pdf');plt.show()
#
# grid = sns.lmplot(x='Conc /uM',y='y',
#             col='Drug',data=df_euclid_df.reset_index(),sharey=False,x_bins=18,
#             markers=None,hue='Drug')
# grid.set(xscale="log")
# plt.savefig('facet_drugs_dose_median.pdf');plt.show()


drug_median_per_image_euclid = pd.DataFrame(\
                                    euclidean_distances(drug_median_per_image),\
                                    index=drug_median_per_image.index)

drug_median_per_conc_euclid =  pd.DataFrame(euclidean_distances(drug_median_per_conc),index=drug_median_per_conc.index)

if(FLAG_MEDIAN_PER_EUCLID):
    # drug_ecluid_df = pd.DataFrame(drug_ecluid,index=drug.index)
    sns.clustermap(drug_median_per_image_euclid);plt.title('Median Euclidian distance per Organoid')
    if(SAVE_FIG):plt.savefig(metadata()+"_"+DRUG+"_median_per_image_euclid.pdf");plt.show()
    # sns.heatmap(drug_median_per_image_euclid)
    sns.clustermap(drug_median_per_conc_euclid);plt.title('Median Euclidian distance per conc')
    if(SAVE_FIG):plt.savefig(metadata()+"_"+DRUG+"_median_per_conc_euclid.pdf");plt.show()

FLAG_CORR_CONC_TO_FEATURE =1
if(FLAG_CORR_CONC_TO_FEATURE):
    df_CONC = df.copy()
    CONC_IDX = df.index.to_frame()['Conc /uM']
    df_CONC['Conc /uM'] = CONC_IDX
    df_corr = df_CONC.groupby('Drug').apply( lambda x: x.corrwith(x['Conc /uM'])).drop(columns='Conc /uM')
    df_corr
    # df_corr
    # wideform_df_corr
    wideform_df_corr = df_corr.T.stack().reset_index()
    wideform_df_corr_named = wideform_df_corr.rename(columns={'level_0':'Features',0:'Correlation'})
    # wideform_df_corr
    # sns.catplot(x='level_0',y=0,col='Drug',kind="bar",legend=False,data=wideform_df_corr);
    # SAVE_FIG = 1

    # wideform_df_corr_named

    kind = 'strip'
    for kind in ['bar','strip']:
        sns.catplot(x='Features',
                    y='Correlation',
                    col='Drug',col_wrap=2,
                    legend=False,kind=kind,
                    data=wideform_df_corr_named);
        plt.xticks([], []);
        metadata()+'_correlation_'+kind+'.pdf'
        if(SAVE_FIG):plt.savefig(metadata()+'_correlation_'+kind+'.pdf');plt.show()


    correlation = pd.DataFrame(df.corrwith(CONC_IDX))
    # correlation.plot(kind='bar')
    # plt.show()
    correlation_scaled = pd.DataFrame(StandardScaler().fit_transform(correlation),index=correlation.index)
    # correlation_scaled
    # sns.barplot(correlation.reset_index())
    # a=1
    # wideform_df_corr_named_index
    wideform_df_corr_named_index = wideform_df_corr_named.copy()
    wideform_df_corr_named_index['Features'] = wideform_df_corr_named_index.index

    # wideform_df_corr_named_index
    # wideform_df_corr_named_index
    # sns.catplot(x='Features',y='Correlation',col='Drug',col_wrap=2,legend=False,kind=kind,data=wideform_df_corr_named_index)

    wideform_df_corr_named_feature_index = wideform_df_corr_named.copy().set_index('Features')
    # wideform_df_corr_named.groupby('Drug').max()

    best_correlator = wideform_df_corr_named.sort_values('Correlation').drop_duplicates(['Drug'],keep='last')
    # best_correlator
    features = wideform_df_corr_named['Features'].unique()


    df_best_correlator = pd.DataFrame()

    for index,row in best_correlator.iterrows():
        temp_df = pd.DataFrame(df_median.stack().xs(row['Drug'],level='Drug',drop_level=False).\
                xs(row['Features'],level=3,drop_level=False))
        # print(a)
        df_best_correlator = pd.concat([df_best_correlator,temp_df])
        # df_best_correlator.append()
    df_best_correlator_sns = df_best_correlator.reset_index().\
                                rename(columns={'level_3':'Feature',0:'Magnitude'})
    # df_best_correlator_sns.xs(key)
    sns.lmplot(x='Conc /uM',
                y='Magnitude',
                col='Feature',
                col_wrap=None,
                sharey=False,
                hue='Drug',data=df_best_correlator_sns)
    if(SAVE_FIG):plt.savefig(metadata()+'_best_correlator.pdf')

    for best_feature in best_correlator['Features']:
        sns.lmplot(x='Conc /uM',
                    col='Drug',
                    col_wrap=None,
                    hue='Drug',
                    y=best_feature,
                    sharey=False,
                    data=df_median[best_feature].reset_index())
        # plt.savefig()
        plt.show()


# %% IC50

FLAG_IC50_FULL =0
if(FLAG_IC50_FULL):
    drug_median_per_image_euclid_full_zero = df_median_euclid_stacked.\
                                                xs(0.0410,level='Conc /uM II').\
                                                droplevel(('ImageNumber II'))

    bool_idx =drug_median_per_image_euclid_full_zero.index.get_level_values('Drug')\
                ==drug_median_per_image_euclid_full_zero.index.get_level_values('Drug II')

    drug_to_drug = drug_median_per_image_euclid_full_zero.loc[bool_idx]
    mean_drug_to_drug = drug_to_drug.groupby(['Drug','Conc /uM','ImageNumber']).mean()
    # mean_drug_to_drug.xs(DRUG,level='Drug').shape

    sns.lmplot(x ='Conc /uM', y='Euclidian distance',
                col='Drug',sharey=False,
                data=mean_drug_to_drug.reset_index())



FLAG_IC50 =1

if(FLAG_IC50):
    drug_median_per_image_abs_euclid= pd.DataFrame(drug_median_per_image.apply(numpy.linalg.norm,axis=1))
    drug_median_per_image_abs_euclid_df= drug_median_per_image_abs_euclid.rename(columns={0:'y'})

    # drug_df_euclid = pd.DataFrame(drug_df.apply(numpy.linalg.norm,axis=1))
    # drug_df_euclid_df = drug_df_eucld.rename(columns={0:'y'})

    df_euclid_median = pd.DataFrame(dimensional_pca_df_median.apply(numpy.linalg.norm,axis=1))
    df_euclid_median_df = df_euclid_median.rename(columns={0:'y'})

    df_euclid = pd.DataFrame(dimensional_pca_df.apply(numpy.linalg.norm,axis=1))
    df_euclid_df = df_euclid.rename(columns={0:'y'})
    SAVE_FIG = 1

    grid = sns.lmplot(x='Conc /uM',y='y',
                col='Drug',data=df_euclid_median_df.reset_index(),
                sharex=True,sharey=False,x_bins=18,
                markers=None,hue='Drug')
    grid.set(xscale="log")
    if(SAVE_FIG):plt.savefig('facet_drugs_dose_median.pdf');plt.show()

    grid = sns.lmplot(x='Conc /uM',y='y',
                col='Drug',data=df_euclid_df.reset_index(),
                sharex=True,sharey=False,x_bins=18,
                markers=None,hue='Drug')
    grid.set(xscale="log")
    if(SAVE_FIG):plt.savefig('facet_drugs_dose.pdf');plt.show()

if(FLAG_IC50):
    # drug_median_per_image_euclid
    # drug_median_per_image_euclid.index.to_frame()['Conc /uM'].unique()
    distance_to_control = drug_median_per_image_euclid.xs(0.0410,level='Conc /uM')
    distance_to_control_indexed = distance_to_control\
                                    .transpose()\
                                    .set_index(drug_median_per_image_euclid.index)\
                                    .droplevel('ImageNumber')
    # from scipy.stats.stats import linregress
    # from sklearn.linear_model import LogisticRegression
    # logreg = LogisticRegression()

    mean_distance_to_control_indexed = pd.DataFrame(distance_to_control_indexed.mean(axis=1).reset_index())

    x_data = mean_distance_to_control_indexed.reset_index()['Conc /uM'].values
    y_data = mean_distance_to_control_indexed.reset_index()[0].values
    if(F)
    sns.violinplot(x='Conc /uM',y=0,data=mean_distance_to_control_indexed).set(ylabel='Euclidean distance')
    plt.savefig(f'{metadata()}_{DRUG}_violinplot.pdf')
    plt.show()

    # kstest(scipy.stats.t.rvs(3,size=1000),'norm')
    from scipy.stats import linregress
    # kstest(y_data,'norm')
    for i in [(np.median,'median','linear'),(np.median,'median','log'),(np.mean,'mean','linear'),(np.mean,'mean','log')]:
        stat,file,axes = i
        m,c,R,pvalue,stderr = linregress(x_data,y_data)
        CI = 2.58*stderr
        CI*100
        # https://www.sciencegateway.org/protocols/cellbio/drug/hcic50.htm
        EC50 = (0.5 - m)/c
        title = f'EC50: {EC50:.2g}±{IC50*CI:.2g} | pvalue: {pvalue:.2g} | R: {R:.2g} | stderr: {stderr:.2g}'
        title = f'95%CI:{CI*100:.2g}% | pvalue: {pvalue:.2g} | R: {R:.2g} | stderr: {stderr:.2g}'
        print(title)
        plot_sns = sns.regplot(x=x_data,y=y_data,x_estimator=stat)
        # plot_sns = sns.regplot(x=x_data,y=y_data)
        plot_sns.set(xscale=axes,xlabel="Conc /uM",ylabel="Euclidian distance to Control Organoids")
        plt.title(title)
        plt.tight_layout()
        if(SAVE_FIG):plt.savefig(metadata()+'_'+DRUG+"_"+file+"_"+axes+"_distance_to_control.pdf")
        plt.show()





# %% ######### PCA ###############
# sns.scatterplot(x='principal component 1',
#                 y="principal component 2",
#                 hue="Conc /uM",data=principalDf.reset_index())
# # CHANNELS =1
# if(SAVE_FIG):plt.savefig("Channels_"+str(CHANNELS)+"_"+DRUG+"_df_pca.pdf");plt.show()
#
# pca = PCA(n_components=2).fit_transform(drug_median_per_image)
# principalDf = pd.DataFrame(data = pca,
#                             columns = ['principal component 1', 'principal component 2'],
#                             index=drug_median_per_image.index)
#
# sns.scatterplot(x='principal component 1',
#                 y="principal component 2",
#                 hue="Conc /uM",data=principalDf.reset_index())
# if(SAVE_FIG):plt.savefig("Channels_"+str(CHANNELS)+"_"+DRUG+"_median_per_image_pca.pdf");plt.show()

######### TSNE
# TSNE_FLAG = 0
if(TSNE_FLAG):
    from sklearn.manifold import TSNE

    TSNE = TSNE(n_components=2).fit_transform(G007_median_per_image)
    principalDf = pd.DataFrame(data = TSNE,
                                columns = ['principal component 1', 'principal component 2'],
                                index=G007_median_per_image.index)

    sns.scatterplot(x='principal component 1',
                    y="principal component 2",
                    hue="Conc /uM",data=principalDf.reset_index())
    if(SAVE_FIG):plt.savefig("G007_median_per_image_tnse.pdf");plt.show()
    #use dimensionality reduction first


'''
Maximum correlation. For a set of n doses for each compound, the NxN correlation matrix is computed between all pairs of concentrations, and the maximum value is used as the dose-independent similarity score72.
'''
G007_df = df.xs(DRUG,level='Drug')
FLAG_MAXCORR = 0
if(FLAG_MAXCORR):
    G007_df
    good = G007_df.unstack(level='Conc /uM').corr().stack()
    G007_df_ImageNumber = G007_df.unstack(level='ImageNumber').corr().stack()
    good = good.replace(1,np.NaN)
    yes = good.groupby(level=[1,2]).max()
    yes
    # aaaa = yes.unstack()
    sns.clustermap(yes)
    aa = G007_df.transpose().corr()
    aa
    aa.unstack()
    aa.reset_index(col_level=2)
    aa.stack(level=0)

    Maximum_correlation_g007_2d
    G007_df.transpose().corr()
    Maximum_correlation_g007_2d = (G007_df.transpose().corr()).groupby('Conc /uM',level='Conc /uM').max()
    Maximum_correlation_g007_2d
    Maximum_correlation_g007 = (G007_df.transpose().corr()).groupby(level='Conc /uM').max()
    Maximum_correlation_g007
    Maximum_correlation_g007_2 = Maximum_correlation_g007.transpose().groupby(level='Conc /uM').max()
    # Maximum_correlation_g007 = (g007.transpose().corr()).groupby(level='Conc /uM').max(axis=0).max(axis=1)
    # (-(g007.transpose().corr().abs()-1)).max().max()
    # disds = (-(g007.transpose().corr()-1)).max().max();disds
    sns.clustermap(Maximum_correlation_g007_2)
    MC = Maximum_correlation_g007_2.replace(1,np.NaN).max().max()
    Maximum_correlation_g007_2.max()
    sns.clustermap(Maximum_correlation_g007)

'''Titration-invariant similarity score. First, the titration series of a compound is built by computing the similarity score between each dose and negative controls. Then, the set of scores is sorted by increasing dose and is split into subseries by using a window of certain size (for instance, windows of three doses). Two compounds are compared by computing the correlation between their subwindows, and only the maximum value is retained83.'''





#https://science.sciencemag.org/content/306/5699/1194
#
# FLAG_TISS = 0
# if(FLAG_TISS):
#     control = g007.xs(list(set(extracted_data['Conc /uM']))[1],level='Conc /uM')
#
#     # euclid_fun = lambda(x): euclidean_distances(x,x)
#     g007.groupby(level='Conc /uM').apply(lambda x: euclidean_distances(x,x))
#
#
#     ecluid_g007 = euclidean_distances(g007,g007)
#     g007_ecluid_df = pd.DataFrame(g007_ecluid,index=g007.index)
#     index = g007_ecluid_df.index
#     euclid_df_indexed = g007_ecluid_df.transpose().set_index(index)
#
#
#     doses = list(set(extracted_data['Conc /uM']))
#     doses = euclid_df_indexed.index.levels[1]
#     euclid_df_indexed.xs(doses[0],level='Conc /uM')
#
#     control = g007.xs(doses[0],level='Conc /uM').iloc[0]
#     drug_1 = g007.xs(doses[0],level='Conc /uM').iloc[1]
#
#     control
#
#     from scipy.stats import ks_2samp
#
#     d_value,p_value = ks_2samp(control,drug_1)
#     d_value
#     # Scale D by population
#
#     #The vectors are then individually scaled by a z-score (standard score) by (x_i - mean(x)) / std(x), where x_i is an element in the vector x.
#     from scipy.stats import zscore
#
#     d_scale = zscore(d_value)
#
    # from rpy2.robjects.packages import importrs
#     base = importr('base')
#     # import R's "utils" package
#     utils = importr('utils')
#     import rpy2.robjects.packages as rpackages
#
#     # import R's utility package
#     utils = rpackages.importr('utils')
#
#     # select a mirror for R packages
#     utils.chooseCRANmirror(ind=1) # select the first mirror in the list
#     # R package names
#     packnames = ('ggplot2', 'hexbin')
#
#     # R vector of strings
#     from rpy2.robjects.vectors import StrVector
#     from rpy2 import robjects
#     # Selectively install what needs to be install.
#     # We are fancy, just because we can.
#     # names_to_install = [x for packnames if not rpackages.isinstalled(x)]
#     # if len(names_to_install) > 0:
#         # utils.install_packages(StrVector(names_to_install))
#
#
#     test = robjects.r('''
#         # create a function `f`
#             f <- function(r, verbose=FALSE) {
#                 if (verbose) {
#                     cat("I am calling f().\n")
#                 }
#                 2 * pi * r
#             }
#             # call the function `f` with argument value 3
#             f(3)
#     ''')
#     test = robjects.r('''
#         # create a function `f`
#             if (!require(devtools)) install.packages('devtools')
#             devtools::install_github('Swarchal/TISS')
#     ''')
#
#
#     test(0)
#
#     # r_f = robjects.globalenv['f']
#     # print(r_f.r_repr())
#     r_f = robjects.r['f']
#     r_f(2)
#
#     tiss = base = importr('Swarchal/TISS')
#     library('TISS')
#
#     utils.install_packages('devtools')
#

# '''
# %load_ext rpy2.ipython
# # %%R -i df -w 5 -h 5 --units in -r 200
# # install.packages('devtools')
# # library('devtools')
# # # install.packages("ggplot2", repos='http://cran.us.r-project.org', quiet=TRUE)
# # #install.packages("ggplot2",repos='http://cran.us.r-project.org')
# # #if (!require(devtools))
# # #install.packages('devtools',repos='http://cran.us.r-project.org')
# # devtools::install_github('Swarchal/TISS')
# '''
