import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Convolution1D, Flatten
from keras.optimizers import SGD
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import pdist
import pandas as pd
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

PREDICTOR = 'Conc /uM'
PREDICTOR = 'Drug'
SAVE_FIG = 1
FIG_PATH = './graphs/ML/'
CHANNELS = 1
# plt.rcParams['figure.figsize'] = (14, 10)

DRUGS = ['Control', 'Dabrafenib', 'G007', '5FU', 'Vemurafenib']
# DRUGS = ['Control', 'G007', '5FU', 'Vemurafenib']
DRUG = DRUGS[2]
DRUGS
predictions = {}


# %%  Read csv

csv_name = 'objects_image.csv'
csv_name = '220519 - G007_objects_image.csv'
csv_name = '190709_objects_image.csv'
csv_name = "./_analysed/230719/df_2Channel.csv"

# df = pd.read_csv(csv_name,index_col=[0,1,2,3])

data_folder = '241019 - ISO49+34'
root_dir = './_analysed/'+data_folder+'/'

metadata = lambda : "./graphs/"+data_folder+"_channels_"+str(CHANNELS)

file = 'df_org+nuclei+image.csv'

df_csv = pd.read_csv(root_dir+file)
df_finite = df_csv.dropna()
df = df_finite.set_index(list(df_csv.columns[np.arange(0, 7)]))

prediction_df = pd.DataFrame()

# live_dead_labels = df.index.to_frame()['Conc /uM'].apply(\
#             lambda x: 'Alive' if x<0.1 else 'Dead' if x >=10 else np.NaN)
# %%  ML loop

for DRUG in DRUGS:
    labels = df.index.to_frame()[PREDICTOR]
    print(f'{DRUG}')
    unique_labels = list(set(labels))
    dictionary = dict(zip(DRUGS, np.arange(0, len(DRUGS))))
    labels_dic = labels.map(dictionary).astype(int)
    labels_cat_df = labels_dic.apply(
        lambda x: np.array(to_categorical(x, labels_dic.max()+1)))

    # live_dead_labels.apply(lambda x:np.array(to_categorical(x,3)))
    # labels = np.array();labels

    test_df = df.xs(DRUG, level='Drug', drop_level=False)
    train_df = df.drop(DRUG, level='Drug')

    test_labels = labels_cat_df.xs(DRUG, level='Drug', drop_level=False)
    train_labels = labels_cat_df.drop(DRUG, level='Drug')

    # labels.apply(dictionary)

    # np.vectorize(dictionary.get)(train_labels)
    # a = (list(),list(test_labels))
    y_test = labels.xs(DRUG, level='Drug').to_numpy()
    y_test = np.vstack(np.array(test_labels)[:]).astype(np.int)
    y_train = np.vstack(np.array(train_labels)[:]).astype(np.int)

    x_train = train_df.to_numpy()
    x_train = StandardScaler().fit_transform(train_df)

    x_test = test_df.to_numpy()
    x_test = StandardScaler().fit_transform(test_df)

    model = Sequential()
    # Dense(64) is a fully-connected layer with 64 hidden units.
    # in the first layer, you must specify the expected input data shape:
    # here, 20-dimensional vectors.
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(DRUGS), activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    # sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    # model.compile(loss='categorical_crossentropy',
    #               optimizer='adam',
    #               metrics=['accuracy'])

    history = model.fit(x_train, y_train,
              epochs=20,
              batch_size=128,
              shuffle=True,
              verbose=1)

    model.evaluate(x_train, y_train, batch_size=128)

    # predictions[DRUG] = model.predict(x_test)
    # test_df
    dictionary_reversed = dict(map(reversed, dictionary.items()))
    predict_current_df = pd.DataFrame(model.predict(x_test), index=test_df.index).\
                            rename(columns=dictionary_reversed)

    prediction_df = prediction_df.append(predict_current_df)

# %% Sundry

df_new = prediction_df.rename(columns=dictionary_reversed)
# ['ImageNumber','ObjectNumber','Date','Drug','Cell','Replicate','Conc /uM']
# df_new.groupby(level=['Cell','Drug','Conc /uM']).count()

df_new
df_new_by_drug_med = df_new.\
            groupby(['ObjectNumber', 'Date', 'Drug',
                        'Cell', 'Replicate', 'Conc /uM']).median().\
                        groupby(level=['Cell', 'Drug']).count()
df_new_by_drug_med

df_new_no_orgs = df_new.groupby(
        ['ImageNumber', 'Date', 'Drug', 'Cell', 'Replicate', 'Conc /uM']).\
        median().\
        groupby(['Drug', 'Cell']).count()


df_new_by_drug_conc = df_new.groupby(level=['Cell', 'Drug', 'Conc /uM']).mean()

df_new_melt = df_new.\
    reset_index().\
    melt(id_vars=['ImageNumber', 'ObjectNumber', 'Date', 'Drug', 'Cell', 'Replicate', 'Conc /uM'],
    var_name="DrugLike", value_name="Similarity")
df_new_melt
g = sns.FacetGrid(df_new_melt, row="Drug", col="DrugLike",
                hue='Conc /uM', margin_titles=True,
                row_order=DRUGS,col_order=DRUGS).\
                    map(plt.hist, "Similarity").add_legend()

df_new_by_drug_avg = df_new.groupby(level=['Cell', 'Drug']).mean();
df_new_by_drug_med = df_new.groupby(level=['Cell', 'Drug']).median();

CONC = 10.0

df_new_by_drug_CONC_avg = df_new.xs(
    CONC, level='Conc /uM').groupby(level=['Cell', 'Drug']).mean()
# df_new_by_drug_CONC_avg
df_new_by_drug_CONC_med = df_new.xs(
    CONC, level='Conc /uM').groupby(level=['Cell', 'Drug']).median()
#
# df_new_by_drug_CON = df_new_by_drug_conc.xs(
#                             CONC, level='Conc /uM')

# CELLS = ['ISO34','ISO49']
# CELL = CELLS[0]
# for CELL in CELLS:
#     data_sub = df_new_by_drug_avg.xs(CELL,level="Cell")
#     sns.heatmap(data_sub, cmap='Spectral')
#     plt.show()
# sns.heatmap(df_new_by_drug_med, cmap='Spectral')

def draw_heatmap(*args, **kwargs):
    data = kwargs.pop('data').drop(columns='Cell')
    g = sns.heatmap(data, **kwargs)
    plt.yticks(rotation=0)
    # g.set_yticklabels(g.get_yticklabels())
facet_heat = lambda df: sns.FacetGrid(df.reset_index(level='Cell'), col='Cell',sharey=False).map_dataframe(draw_heatmap, cmap='Spectral',cbar_kws={'label': 'Similarity'})

facet_heat(df_new_by_drug_med)
if(SAVE_FIG):plt.savefig(metadata()+f'_ml_med_conc_all_similiarity.pdf')
plt.show()
facet_heat(df_new_by_drug_avg)
if(SAVE_FIG):plt.savefig(metadata()+f'_ml_avg_conc_all_similiarity.pdf')
plt.show()
facet_heat(df_new_by_drug_CONC_avg)
if(SAVE_FIG):plt.savefig(metadata()+f'_ml_avg_conc_{CONC}_similiarity.pdf')
plt.show()
facet_heat(df_new_by_drug_CONC_med)
if(SAVE_FIG):plt.savefig(metadata()+f'_ml_med_conc_{CONC}_similiarity.pdf')
plt.show()

df_new_by_drug_conc_melt = pd.melt(df_new_by_drug_conc.reset_index(),
        id_vars=['Drug', 'Conc /uM'],
        value_vars=DRUGS, var_name='DrugLike', value_name='Similarity')

# sns.lmplot(data=df_new_by_drug_conc_melt, x='Conc /uM', y='Similarity',
#             col='Drug', row='DrugLike', sharex=False, sharey=False)
#
# sum_obj_df_new = df_new.unstack(['Drug', 'Conc /uM']).\
#                     groupby('ObjectNumber').\
#                     sum().stack(['Drug', 'Conc /uM'])
#
# avg_obj_df_new = df_new.unstack(['Drug', 'Conc /uM']).\
#                     groupby('ObjectNumber').\
#                     mean().stack(['Drug', 'Conc /uM'])
# avg_obj_df_new
#


# aa = sum_obj_df_new.unstack(['Drug']).groupby('Conc /uM').mean().stack(['Drug']).stack()
# data_de = aa.reset_index()
# sns.relplot(x='Conc /uM', col='level_2',y=0,hue='Drug',data=data_de)
#
# df_new_maxed = df_new.idxmax(axis=1)
#
# # pd.melt(df_new_maxed, id_vars=DRUGS, value_vars=['B'],\
# #          var_name='myVarname', value_name='myValname')
#
# pd.DataFrame(df_new_maxed).pivot(0)
# df_new_maxed_drug = df_new_maxed.groupby(['Drug','Conc /uM']).count()
# df_new_maxed_drug = pd.DataFrame(df_new_maxed)
# df_new_maxed.reset_index()
# aa = df_new_maxed.reset_index()
# hh = pd.DataFrame(df_new_maxed)
# hh[1]=1
#
# good = hh.groupby(['Drug',0,'Conc /uM']).count()
#
# sns.relplot(x=0,col='Conc /uM',col_wrap=2,y=1,hue='Drug',data=good.reset_index())
#
# df_new_maxed.value_counts()
# df_new_maxed.value_counts()
#
# df_new_maxed.value_counts()
# df_new_maxed_drug_conc = pd.DataFrame(df_new_maxed).unstack('Drug').groupby('Conc /uM').count()

# df_new_maxed_drug_conc
# df_new_maxed.pivot(columns=DRUGS, values=0)


## DATA VIS #######
# df_new = pd.DataFrame.from_dict(predictions,orient='index')

# predictions
# df_new.xs('Control').to_numpy()[0].shape
# '''''''''''''''''
# df_new
#
# avg_obj_df_new
#
#
# sum_df = df_new.applymap(lambda x: np.sum(x,axis=0)/x.shape[0])
# avg_obj_df_new
# items_as_cols = sum_df.apply(lambda x: pd.Series(x[0]), axis=1)
# items_as_cols.columns=DRUGS
#
# items_as_cols
# mask = np.zeros_like(items_as_cols_reordered)
# mask[np.triu_indices_from(mask)] = True
# # mask[np.tril_indices_from(mask)] = True
#
# sns.heatmap(items_as_cols,mask=mask,cmap='Spectral')
# if(SAVE_FIG):plt.savefig(FIG_PATH+'ML_predictor_similarity.pdf')
# items_as_cols
# # sns.clustermap(items_as_cols,mask=mask,cmap='Spectral',col_cluster=False,row_linkage=linkage_matrix)
# sns.clustermap(items_as_cols,mask=mask,cmap='Spectral',col_cluster=False)
# if(SAVE_FIG):plt.savefig(FIG_PATH+'ML_predictor_similarity_clustered.pdf')
# df_median = df.groupby(level='Drug').median()
# df_median=df_median.reindex(index=DRUGS)
#
# # upper = np.mean(df.values.flatten())+ 3*np.std(df.values.flatten());upper
# # lower = np.mean(df.values.flatten())- 3*np.std(df.values.flatten());lower
#
# df_median_scaler = df_median.copy()
# df_median_scaler.iloc[::] = StandardScaler().fit_transform(df_median)
# df_median_scaler
# # df_median_scaler
# # items_as_cols_reordered
# # df_median_scaler
# # df_median.reindex(index=DRUGS)
# sns.clustermap(df_median_scaler,
#                 cmap='Spectral',
#                 col_cluster=False,
#                 xticklabels=0,
#                 z_score=0)
# if(SAVE_FIG):plt.savefig(FIG_PATH+'Clustering_of_fingerprints_standard.pdf')
#
# linkage_matrix = linkage(pdist(items_as_cols), 'single');
# sns.clustermap(df_median_scaler,
#                 cmap='Spectral',
#                 col_cluster=False,
#                 row_linkage=linkage_matrix,
#                 xticklabels=0,
#                 z_score=0
#                 )
# if(SAVE_FIG):plt.savefig(FIG_PATH+'Clustering_of_fingerprints_single_ML.pdf')
#
#
# from sklearn.decomposition import PCA
#
# pca_df = df
# index_pca = pca_df.index
# scaled_df_pca = pd.DataFrame(StandardScaler().fit_transform(pca_df),index=pca_df.index)
# pca = PCA(n_components=2)
# pca_model=pca.fit(scaled_df_pca)
# principalComponents = pca_model.transform(scaled_df_pca)
#
# principalDf = pd.DataFrame(data = principalComponents
#              , columns = ['principal component 1', 'principal component 2'],index=pca_df.index)
#
# xx,yy = np.meshgrid(np.linspace(principalComponents[:,0].min(),principalComponents[:,0].max(),500),
#                     np.linspace(principalComponents[:,1].min(),principalComponents[:,1].max(),500))
#
# xx_yy = np.column_stack((xx.flatten(),yy.flatten()))
#
# fingerprints_from_pca_space = PCA.inverse_transform(pca_model,xx_yy)
# labels_pca_space = model.predict(fingerprints_from_pca_space)
# labels_pca_space_as_num = np.argmax(labels_pca_space,axis=1)
#
# Z = labels_pca_space_as_num.reshape(xx.shape)
# hue = 'Drug'
# plt.imshow(Z, interpolation='nearest',
#             extent=(xx.min(), xx.max(), yy.min(), yy.max()),
#             cmap='Pastel2',
#             aspect='auto', origin='lower')
# sns.scatterplot(x='principal component 1',
#                 y="principal component 2",hue=hue,data=principalDf.reset_index())
