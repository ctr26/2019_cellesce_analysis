import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Convolution1D, Flatten
from keras.optimizers import SGD
from keras.utils import to_categorical

import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

CATEGORICAL = 1

df = pd.read_csv('objects_image.csv',index_col=[0,1,2,3]);df

# df
# df = df.unstack(level=['Drug','Conc /uM']).groupby(level='ImageNumber').median().stack(level=['Conc /uM','Drug'])

labels = np.array(df.index.to_frame()['Drug']);labels
unique_labels = list(set(labels))
dictionary = dict(zip(unique_labels,np.arange(0,len(unique_labels))))
# labels.apply(dictionary)

if(not(CATEGORICAL)):
    y = np.log10(labels)
if(CATEGORICAL):
    y = to_categorical(np.vectorize(dictionary.get)(labels))

x = df.to_numpy()
x = StandardScaler().fit_transform(df)

x_train, x_test, y_train, y_test = train_test_split(x,y,test_size=0.33)

# y_train.shape
# y_train.shape
# x_train = np.random.random((1000, 20))
# y_train = keras.utils.to_categorical(np.random.randint(10, size=(1000, 1)), num_classes=10)
# x_test = np.random.random((100, 20))
# y_test = keras.utils.to_categorical(np.random.randint(10, size=(100, 1)), num_classes=10)


model = Sequential()
# Dense(64) is a fully-connected layer with 64 hidden units.
# in the first layer, you must specify the expected input data shape:
# here, 20-dimensional vectors.
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
if(not(CATEGORICAL)):
    model.add(Dense(1, activation='relu'))
if(CATEGORICAL):
    model.add(Dense(len(unique_labels), activation='softmax'))

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
if(CATEGORICAL):
    loss = 'categorical_crossentropy'
if(not(CATEGORICAL)):
    loss = 'mean_squared_error'

model.compile(loss=loss,
              optimizer='adam',
              metrics=['accuracy'])
#
#
#
#
#
# sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
# model.compile(loss='categorical_crossentropy',
#               optimizer='adam',
#               metrics=['accuracy'])

history = model.fit(x_train, y_train,
          epochs=50,
          batch_size=128,
          verbose=1)


score = model.evaluate(x_test, y_test, batch_size=128);score

y_predict = model.predict(x_test).flatten()
y_test
(y_predict-y_test)/(2*(y_predict+y_test))
from sklearn.metrics import mean_absolute_error

error = mean_absolute_error(y_test,y_predict)#(y_predict-y_test)/(2*(y_predict+y_test))


error_ratio = y_predict/y_test

error_ratio_clean = error_ratio[~np.isnan(error_ratio)]
error_ratio_cleaner = error_ratio[~np.isinf(error_ratio)]
y_predict.shape
y_test.shape
import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
plt.hist(error_cleaner,bins=1000)
plt.xlim([0,100])
ax.set_xscale('symlog')
plt.show()
# import matplotlib.pyplot as plt
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
plt.hist(error)
# plt.xlim([0,100])
# ax.set_xscale('symlog')
plt.show()

plt.plot(history.history['acc'])

history
