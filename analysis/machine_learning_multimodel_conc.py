import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Convolution1D, Flatten
from keras.optimizers import SGD
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import pdist
import pandas as pd

PREDICTOR = 'Conc /uM'
PREDICTOR = 'Drug'
SAVE_FIG = 1



plt.rcParams['figure.figsize'] = (10,7)

DRUGS = ['Control', 'Dabrafenib', 'G007', '5FU', 'Vemurafenib']
# DRUGS = ['Control', 'G007', '5FU', 'Vemurafenib']
DRUG = DRUGS[2]
CHANNELS = 2

predictions = {}

# csv_name = 'objects_image.csv'
# csv_name = '220519 - G007_objects_image.csv'
# csv_name = '190709_objects_image.csv'
data_folder = '230719'
metadata = lambda : "./graphs/"+data_folder+"_channels_"+str(CHANNELS)
FIG_PATH = metadata()+'_ML_'

csv_name = './_analysed/'+data_folder+'/df_2Channel.csv'
# df
df = pd.read_csv(csv_name,index_col=[0,1,2,3])
# df
prediction_df = pd.DataFrame()

# live_dead_labels = df.index.to_frame()['Conc /uM'].apply(\
#             lambda x: 'Alive' if x<0.1 else 'Dead' if x >=10 else np.NaN)

for DRUG in DRUGS:
    labels = df.index.to_frame()[PREDICTOR]

    unique_labels = list(set(labels))
    dictionary = dict(zip(DRUGS,np.arange(0,len(DRUGS))))
    labels_dic = labels.map(dictionary)
    labels_cat_df = labels_dic.apply(lambda x:np.array(to_categorical(x,labels_dic.max()+1)))

    # live_dead_labels.apply(lambda x:np.array(to_categorical(x,3)))

    # labels = np.array();labels

    test_df= df.xs(DRUG,level='Drug',drop_level=False)
    train_df=df.drop(DRUG,level='Drug')

    test_labels = labels_cat_df.xs(DRUG,level='Drug',drop_level=False)
    train_labels =labels_cat_df.drop(DRUG,level='Drug')

    # labels.apply(dictionary)

    # np.vectorize(dictionary.get)(train_labels)
    # a = (list(),list(test_labels))
    y_test = labels.xs(DRUG,level='Drug').to_numpy()
    y_test = np.vstack(np.array(test_labels)[:]).astype(np.int)
    y_train = np.vstack(np.array(train_labels)[:]).astype(np.int)


    x_train = train_df.to_numpy()
    x_train = StandardScaler().fit_transform(train_df)

    x_test = test_df.to_numpy()
    x_test = StandardScaler().fit_transform(test_df)


    model = Sequential()
    # Dense(64) is a fully-connected layer with 64 hidden units.
    # in the first layer, you must specify the expected input data shape:
    # here, 20-dimensional vectors.
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(DRUGS), activation='softmax'))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    # sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    # model.compile(loss='categorical_crossentropy',
    #               optimizer='adam',
    #               metrics=['accuracy'])

    history=model.fit(x_train, y_train,
              epochs=50,
              batch_size=128,
              shuffle=True,
              verbose=0)


    model.evaluate(x_train, y_train, batch_size=128)

    # predictions[DRUG] = model.predict(x_test)
    test_df
    prediction_df = prediction_df.append(pd.DataFrame(model.predict(x_test),index=test_df.index))

prediction_df
df_new = prediction_df.rename(columns = dict(zip([0,1,2,3,4],DRUGS)))

predictions = df_new.apply(lambda x: x.argmax(),axis=1)
predictions_counted = predictions.groupby(['Drug','Conc /uM']).value_counts()
predictions_counted_drugs=predictions_counted.groupby(level=[0,2]).sum()
predictions_counted_drugs
predictions_counted_drugs_norm = predictions_counted_drugs.groupby('Drug').apply(lambda x:x/(x.sum()))
predictions_counted_drugs_norm_unstack = predictions_counted_drugs_norm.unstack().drop('Control')
predictions_counted_drugs_norm_unstack
# mask = np.zeros_like(predictions_counted_drugs_norm_unstack)
# mask[np.triu_indices_from(mask)] = 1
# mask
# mask[np.tril_indices_from(mask)] = True
# predictions_counted_drugs_norm_unstack
sns.heatmap(predictions_counted_drugs_norm_unstack,cmap='Spectral',cbar_kws={'label': 'Normalised similarity score'})
if(SAVE_FIG):plt.savefig(FIG_PATH+'predictor_similarity.pdf')

sns.clustermap(predictions_counted_drugs_norm_unstack.fillna(0,axis=0),cmap='Spectral',col_cluster=False)
if(SAVE_FIG):plt.savefig(FIG_PATH+'predictor_similarity_clustered.pdf')

df_median = df.groupby(level='Drug').median()
df_median=df_median.reindex(index=DRUGS)

# upper = np.mean(df.values.flatten())+ 3*np.std(df.values.flatten());upper
# lower = np.mean(df.values.flatten())- 3*np.std(df.values.flatten());lower

#%% Similarity as a function of conc

predictions_drugs_norm = predictions_counted.groupby(level=[0,1]).apply(lambda x:x/(x.sum()))
predictions_drugs_norm_to_control = predictions_drugs_norm.xs('Control',level=2)
sns.lmplot(x='Conc /uM',
        y='Similarity',
        col='Drug',hue='Drug',
        sharey=False,sharex=False,
        data=predictions_drugs_norm_to_control.reset_index().rename(columns={0:'Similarity'}))
predictions_drugs_norm_to_control.reset_index()
predictions_drugs_norm_to_control
#%% Fingerprirts

df_median_scaler = df_median.copy()
df_median_scaler.iloc[::] = StandardScaler().fit_transform(df_median)

sns.clustermap(df_median_scaler,
                cmap='Spectral',
                col_cluster=False,
                xticklabels=0,
                z_score=0)
if(SAVE_FIG):plt.savefig(FIG_PATH+'Clustering_of_fingerprints_standard.pdf')

linkage_matrix = linkage(pdist(predictions_counted_drugs_norm_unstack.fillna(0,axis=0)), 'single');
sns.clustermap(df_median_scaler,
                cmap='Spectral',
                col_cluster=False,
                row_linkage=linkage_matrix,
                xticklabels=0,
                z_score=0
                )
if(SAVE_FIG):plt.savefig(FIG_PATH+'Clustering_of_fingerprints_single_ML.pdf')

# PCA, won't work though
# # from sklearn.decomposition import PCA
# #
# # pca_df = df
# # index_pca = pca_df.index
# # scaled_df_pca = pd.DataFrame(StandardScaler().fit_transform(pca_df),index=pca_df.index)
# # pca = PCA(n_components=2)
# # pca_model=pca.fit(scaled_df_pca)
# # principalComponents = pca_model.transform(scaled_df_pca)
# #
# # principalDf = pd.DataFrame(data = principalComponents
# #              , columns = ['principal component 1', 'principal component 2'],index=pca_df.index)
# #
# # xx,yy = np.meshgrid(np.linspace(principalComponents[:,0].min(),principalComponents[:,0].max(),500),
# #                     np.linspace(principalComponents[:,1].min(),principalComponents[:,1].max(),500))
# #
# # xx_yy = np.column_stack((xx.flatten(),yy.flatten()))
# #
# # fingerprints_from_pca_space = PCA.inverse_transform(pca_model,xx_yy)
# # labels_pca_space = model.predict(fingerprints_from_pca_space)
# # labels_pca_space_as_num = np.argmax(labels_pca_space,axis=1)
# #
# # Z = labels_pca_space_as_num.reshape(xx.shape)
# # hue = 'Drug'
# # plt.imshow(Z, interpolation='nearest',
# #             extent=(xx.min(), xx.max(), yy.min(), yy.max()),
# #             cmap='Pastel2',
# #             aspect='auto', origin='lower')
# # sns.scatterplot(x='principal component 1',
# #                 y="principal component 2",hue=hue,data=principalDf.reset_index())
