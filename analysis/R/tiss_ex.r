library('TISS')
library('readr')
# install.packages("pvclust")
library(pvclust)

# install.packages("readr")
# install.packages('devtools')
# devtools::install_github('Swarchal/')

#csv <- read_csv("OneDrive - National Physical Laboratory/_projects/2019_cellesce/objects_image")

root_dir <- '~/OneDrive - National Physical Laboratory/_projects/2019_cellesce/_analysed/230719/'

#files= c('objects.csv','median_per_organoid_objects_image.csv','objects_image.csv','median_per_organoid_objects.csv')
files =  c('df_2Channel_median_per_org_.csv','df_2Channel.csv')
tiss_list <- list()

for (file in files){
    print(file)
  csv <- read_csv(paste(root_dir,file,sep = ""))

  data <- na.omit(csv);data
  ex_data <- as.data.frame(data)
  #data(checkme)

  metadata <- construct_metadata(ex_data,
                                 compound_col = 'Drug',
                                 conc_col = 'Conc /uM',
                                 feature_cols = 5:ncol(csv)-1,
                                 negative_control = "Control")

  # compound_data <- get_compound_data(ex_data, metadata)
  # negative_control <- get_negative_control(ex_data, metadata)
  # d_out <- calculate_d(compound_data, negative_control)
  # d_scale <- scale_d(d_out)
  # out <- correlate(d_scale, metadata)
  # ans <- trim(d_scale, out, metadata = metadata)
  # similarity_list(ans)
  # ex_data
  #result <- pvclust(ex_data, method.dist="cor", method.hclust="average", nboot=1000)

  tiss_list[[file]] <- tiss(ex_data, metadata)
  plot(hclust(tiss_list[[file]]),main=file)
}
