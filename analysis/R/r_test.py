
from rpy2.robjects.packages import importr
import rpy2.interactive as r
import rpy2.robjects as robjects
import rpy2.robjects.packages as r
import rpy2.robjects

ks_test = rpy2.robjects.r('ks.test')

utils = importr("utils")
TISS = importr("TISS")
Rclusterpp = importr("Rclusterpp")
devtools = importr("devtools")
importr("devtools")
utils.install_packages('devtools')

?devtools
devtools.

install_github('Swarchal/')

robjects.r('''
            devtools::install_github('Swarchal/TISS')
            '''
            )

hasattr(devtools,"")

%who *

import rpy2.robjects.packages as rpackages
utils = rpackages.importr('utils')

utils.chooseCRANmirror(ind=1) # select the first mirror in the list

packnames = ('ggplot2', 'devtools')
from rpy2.robjects.vectors import StrVector
utils.install_packages(StrVector(packnames))
utils.update_packages(checkBuilt = True, ask = False)

robjects.r('''
    if (!require(devtools)) install.packages('devtools')
    devtools::install_github('Swarchal/TISS')
    ''')


%load_ext rpy2.ipython
%%R
library('devtools')
library('ggplot2')
