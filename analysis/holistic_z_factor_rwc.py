import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import os
os.getcwd()

working_dir = "./_mnt/GoogleDrive/My Drive/NPL/2019_cellesce/analysis/"
os.chdir(os.path.expanduser("~"))
os.chdir(working_dir)

dir = './_analysed/041019 - rwc/'
file = 'OrganoidRecord.txt'
data = pd.read_csv(dir + file,delimiter='\t')
data_melt = pd.melt(data,id_vars=data.columns[[0,1,2,3]],
                        value_vars=data.columns[4:],
                        var_name='Measurand',
                        value_name='Measurement')
# data_melt_iso34 = data_melt[data_melt['Cell']=='ISO34']
f = sns.relplot(x='Conc',y='Measurement',hue='Measurand',col='Drug',style='Cell',
                kind='line',data=data_melt)
f.set(xscale="log", yscale="log")
f.savefig(dir+'line_plots.pdf')
data_melt_indexed = data_melt.set_index(data_melt.columns[[0,1,2,3,4]].to_list())

negative_control = data_melt_indexed.xs(0,level='Conc',drop_level=True).droplevel(['Organoid','Drug'])
data_melt_indexed_dropped = data_melt_indexed.droplevel(['Organoid'])

negative = negative_control.groupby(negative_control.index.names).agg([np.mean, np.std])
positive = data_melt_indexed_dropped.groupby(data_melt_indexed_dropped.index.names).agg([np.mean, np.std])

sigma_p = positive['Measurement','std']
sigma_n = negative['Measurement','std']

mu_p = positive['Measurement','mean']
mu_n = negative['Measurement','mean']


def zfactor(sigma_n,sigma_p,mu_p,mu_n):
    return 1-(3*((sigma_p+sigma_n)/np.abs(mu_p-mu_n)))


z_out = zfactor(sigma_n,sigma_p,mu_p,mu_n)
z_out_df = z_out.dropna()
z_out.dropna()
effective = z_out[z_out>0];effective
