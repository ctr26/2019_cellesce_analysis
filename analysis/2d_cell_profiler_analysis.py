# https://www.nature.com/articles/nmeth.4397


import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
sns.set()

object_file_path = "./_analysed/G007/Nuclei.csv"
image_file_path = "./_analysed/G007/Image.csv"

object_file_df = pd.read_csv(object_file_path);object_file_df.head()
image_file_df = pd.read_csv(image_file_path);image_file_df.head()
merged_full_data_set = object_file_df.merge(image_file_df, on='ImageNumber', how='left');merged_full_data_set.head()


# pattern = r'(_ISO34_)(?P<Drug>[\w]*_)(?P<Concentration>[\w]*[uM])'
pattern = r'ISO34_(?P<Drug>[^_\s]+)[_\s](?P<Concentration>[\w\d]*uM)'
filenames = merged_full_data_set['PathName_Channels'];filenames
extracted_data = filenames.str.extract(pattern)
extracted_data['Conc /uM'] = pd.to_numeric((extracted_data['Concentration'].str.replace('_','.')).str.replace('uM',''))


merged_full_data_set_extracted = merged_full_data_set.join(extracted_data);merged_full_data_set_extracted
# Left merge
# merged_full_data_set = object_file_df.merge(image_file_df_extracted, on='ImageNumber', how='left')

# Dimensionality reduction
from sklearn.preprocessing import StandardScaler

scaled_object_file_df = StandardScaler().fit_transform(object_file_df)

from sklearn.decomposition import PCA
pca = PCA(n_components=2)
principalComponents = pca.fit_transform(scaled_object_file_df)
principalDf = pd.DataFrame(data = principalComponents
             , columns = ['principal component 1', 'principal component 2'])
pca_df_meta = principalDf.join(merged_full_data_set_extracted[['ImageNumber','Conc /uM','Drug']])

#
sns.scatterplot(x='principal component 1',y="principal component 2",hue="Conc /uM",data=pca_df_meta)
g = sns.FacetGrid(pca_df_meta,col='Drug',hue="Conc /uM")
g.map(sns.scatterplot,"principal component 1","principal component 2")
g.add_legend();

#######
FLAGS_TSNSE = 0
if(FLAGS_TSNSE):
    from sklearn.manifold import TSNE

    #
    # principalDf['ImageNumber'] = object_file_df['ImageNumber']
    # principalDf
    # #
    # sns.scatterplot(x='principal component 1',y="principal component 2",hue="ImageNumber",data=principalDf)
    # TSNEDf = pd.DataFrame(data = X_embedded, columns = ['principal component 1', 'principal component 2'])
    # #
    # TSNEDf
    # # sns.scatterplot(x='principal component 1',y="principal component 2",data=TSNEDf.reset_index())


    # pca = PCA(n_components=2)
    X_embedded = TSNE(n_components=2).fit_transform(scaled_object_file_df)
    # principalComponents = pca.fit_transform(scaled_object_file_df)
    X_embeddedDf = pd.DataFrame(data = X_embedded, columns = ['tsne component 1', 'tsne component 2'])
    tnse_df_meta = X_embeddedDf.join(merged_full_data_set_extracted[['ImageNumber','Conc /uM','Drug']])

    # sns.scatterplot(x='principal component 1',y="principal component 2",hue="ImageNumber",data=X_embeddedDf)

    sns.scatterplot(x='principal component 1',y="principal component 2",hue="Conc /uM",data=tnse_df_meta)
    g = sns.FacetGrid(tnse_df_meta,col='Drug',hue="Conc /uM")
    g.map(sns.scatterplot,"principal component 1","principal component 2")
    g.add_legend();

########

# fig = plt.figure()
# ax = fig.add_subplot(1,1,1)
# ax.set_xlabel('Principal Component 1', fontsize = 15)
# ax.set_ylabel('Principal Component 2', fontsize = 15)
# ax.set_title('2 component PCA', fontsize = 20)
# # targets = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
# # colors = ['r', 'g', 'b']
# ax.scatter(principalDf['principal component 1'],principalDf['principal component 2'])
# # ax.legend(targets)
# # ax.grid()
# plt.show()


pca = PCA(n_components=0.99, whiten=True)
X_pca = pd.DataFrame(pca.fit_transform(scaled_object_file_df))
X_pca['ImageNumber'] = object_file_df['ImageNumber']
# X_pca.set_index(keys='ImageNumber',inplace=True)
X_pca = X_pca.set_index(['ImageNumber',X_pca.index])



# X_pca
# X_pca.shape
# import numpy as np
# plt.imshow(X_pca[0:2000], interpolation='nearest', aspect='auto',cmap='hsv')
# plt.imshow(X_pca[2000:4000], interpolation='nearest', aspect='auto',cmap='hsv')
# plt.imshow(X_pca[4000:6000], interpolation='nearest', aspect='auto',cmap='hsv')


# sns.clustermap(X_pca)
X_pca.shape
for i in range(1,10):
    sns.heatmap(X_pca.xs(i))
    plt.show()

x_pca_median_per_image = X_pca.groupby(level=0).median()
# x_pca_median_per_image.shape
# extracted_data.shape

extracted_data
x_pca_median_per_image_detailed = pd.concat([x_pca_median_per_image.reset_index(),extracted_data],axis=1)
x_pca_median_per_image_detailed
extracted_data
x_pca_median_per_image_detailed_indexed = x_pca_median_per_image_detailed.set_index(['ImageNumber','Drug','Conc /uM'])

x_pca_median_per_image_detailed_indexed

g007 = x_pca_median_per_image_detailed_indexed.xs('G007',level='Drug').drop('Concentration',axis=1)
g007
g007_corr = g007.corrwith(g007,axis=0)

image_wise_correlation = x_pca_median_per_image.T.corr(method='pearson')
image_wise_correlation
sns.heatmap(image_wise_correlation)

from sklearn.metrics.pairwise import euclidean_distances

x_pca_median_per_image

x_pca_median_per_image_euclid = euclidean_distances(x_pca_median_per_image,x_pca_median_per_image)
g007.values.shape
g007.values
g007.index
extracted_data
pd.DataFrame

g007_ecluid = euclidean_distances(g007,g007)
g007_ecluid_df = pd.DataFrame(g007_ecluid,index=g007.index)
sns.clustermap(g007_ecluid_df)
sns.heatmap(x_pca_median_per_image_euclid)

sns.clustermap(image_wise_correlation)
sns.clustermap(x_pca_median_per_image_euclid)
# X_pca.groupby(level=0)
# Take median for each feature of each organoid

# sns.PairGrid(data=current_imagenumber_df.xs(0,100))



pca = PCA(n_components=2)

principalComponents = pca.fit_transform(g007)
principalDf
principalDf = pd.DataFrame(data = principalComponents
             , columns = ['principal component 1', 'principal component 2'],index=g007.index)
#
principalDf
sns.scatterplot(x='principal component 1',y="principal component 2",hue="Conc /uM",data=principalDf.reset_index())

g007


sns.lineplot(data=principalDf.reset_index(),y='principal component 1',x='Conc /uM')
sns.lineplot(data=principalDf.reset_index(),y='principal component 2',x='Conc /uM')
import numpy as np
ecluid_g007_origin = g007_ecluid_df.apply(lambda x: np.sqrt(np.sum(x)**2),axis=1)
sns.lineplot(data=ecluid_g007_origin.reset_index(),y=0,x='Conc /uM')

ecluid_g007_origin


from sklearn.manifold import TSNE


principalDf['ImageNumber'] = object_file_df['ImageNumber']
principalDf
#
sns.scatterplot(x='principal component 1',y="principal component 2",hue="ImageNumber",data=principalDf)
TSNEDf = pd.DataFrame(data = X_embedded, columns = ['principal component 1', 'principal component 2'])
#
TSNEDf
# sns.scatterplot(x='principal component 1',y="principal component 2",data=TSNEDf.reset_index())


# pca = PCA(n_components=2)
X_embedded = TSNE(n_components=2).fit_transform(scaled_object_file_df)
# principalComponents = pca.fit_transform(scaled_object_file_df)
X_embeddedDf = pd.DataFrame(data = X_embedded, columns = ['principal component 1', 'principal component 2'])
X_embeddedDf['ImageNumber'] = object_file_df['ImageNumber']

sns.scatterplot(x='principal component 1',y="principal component 2",hue="ImageNumber",data=X_embeddedDf)

#use dimensionality reduction first


'''
Maximum correlation. For a set of n doses for each compound, the NxN correlation matrix is computed between all pairs of concentrations, and the maximum value is used as the dose-independent similarity score72.
'''
Maximum_correlation_g007 = (g007.transpose().corr()).groupby(level='Conc /uM').max()
Maximum_correlation_g007
Maximum_correlation_g007_2 = Maximum_correlation_g007.transpose().groupby(level='Conc /uM').max()
Maximum_correlation_g007 = (g007.transpose().corr()).groupby(level='Conc /uM').max(axis=0).max(axis=1)
(-(g007.transpose().corr().abs()-1)).max().max()
disds = (-(g007.transpose().corr()-1)).max().max();disds
sns.heatmap(Maximum_correlation_g007_2)
sns.heatmap(Maximum_correlation_g007)

'''Titration-invariant similarity score. First, the titration series of a compound is built by computing the similarity score between each dose and negative controls. Then, the set of scores is sorted by increasing dose and is split into subseries by using a window of certain size (for instance, windows of three doses). Two compounds are compared by computing the correlation between their subwindows, and only the maximum value is retained83.'''
g007
control = g007.xs(list(set(extracted_data['Conc /uM']))[1],level='Conc /uM')

# euclid_fun = lambda(x): euclidean_distances(x,x)
g007.groupby(level='Conc /uM').apply(lambda x: euclidean_distances(x,x))


ecluid_g007 = euclidean_distances(g007,g007)
g007_ecluid_df = pd.DataFrame(g007_ecluid,index=g007.index)
index = g007_ecluid_df.index
euclid_df_indexed = g007_ecluid_df.transpose().set_index(index)


doses = list(set(extracted_data['Conc /uM']))
doses = euclid_df_indexed.index.levels[1]
euclid_df_indexed.xs(doses[0],level='Conc /uM')

control = g007.xs(doses[0],level='Conc /uM').iloc[0]
drug_1 = g007.xs(doses[0],level='Conc /uM').iloc[1]

control

from scipy.stats import ks_2samp

d_value,p_value = ks_2samp(control,drug_1)
d_value
# Scale D by population

#The vectors are then individually scaled by a z-score (standard score) by (x_i - mean(x)) / std(x), where x_i is an element in the vector x.
from scipy.stats import zscore

d_scale = zscore(d_value)

import r2py
