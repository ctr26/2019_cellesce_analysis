import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Convolution1D, Flatten
from keras.optimizers import SGD
from keras.utils import to_categorical

import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler


df = pd.read_csv('objects_image.csv',index_col=[0,1,2,3]);df

# df = df.unstack(level=['Drug','Conc /uM']).groupby(level='ImageNumber').median().stack(level=['Conc /uM','Drug'])
labels = df.index.to_frame()['Drug']
labels
unique_labels
unique_labels = list(set(labels))
dictionary = dict(zip(unique_labels,np.arange(0,len(unique_labels))))
labels_dic = labels.map(dictionary)
labels_cat_df = labels_dic.apply(lambda x:np.array(to_categorical(x,labels_dic.max()+1)))

labels_cat_df
labels_cat_df.shape
df.shape
labels_cat_df.shape

df.xs('G007',level='Drug').shape
labels_cat_df.drop('G007',level='Drug').shape
# labels = np.array();labels
test_df= df.xs('G007',level='Drug')
train_df=df.drop('G007',level='Drug')

test_labels = labels_cat_df.xs('G007',level='Drug');test_labels
train_labels =labels_cat_df.drop('G007',level='Drug')

# labels.apply(dictionary)

# np.vectorize(dictionary.get)(train_labels)
# a = (list(),list(test_labels))

# y_test = labels.xs('G007',level='Drug').to_numpy()
# test_labels

y_test = np.vstack(np.array(test_labels)[:]).astype(np.int)
y_train = np.vstack(np.array(train_labels)[:]).astype(np.int)

x_train = train_df.to_numpy()
x_train = StandardScaler().fit_transform(train_df)

x_test = test_df.to_numpy()
x_test = StandardScaler().fit_transform(test_df)

y_test.shape
x_test.shape

# y_train.shape
# y_train.shape
# x_train = np.random.random((1000, 20))
# y_train = keras.utils.to_categorical(np.random.randint(10, size=(1000, 1)), num_classes=10)
# x_test = np.random.random((100, 20))
# y_test = keras.utils.to_categorical(np.random.randint(10, size=(100, 1)), num_classes=10)


model = Sequential()
# Dense(64) is a fully-connected layer with 64 hidden units.
# in the first layer, you must specify the expected input data shape:
# here, 20-dimensional vectors.
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(len(unique_labels), activation='softmax'))

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
#
#
#
#
#
# sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
# model.compile(loss='categorical_crossentropy',
#               optimizer='adam',
#               metrics=['accuracy'])

history=model.fit(x_train, y_train,
          epochs=50,
          batch_size=128,
          shuffle=True,
          verbose=1)

x_test.shape
y_test.shape


score = model.evaluate(x_test, y_test, batch_size=128);score
score

model.evaluate(x_train, y_train, batch_size=128)
