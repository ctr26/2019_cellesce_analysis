import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Convolution1D, Flatten
from keras.optimizers import SGD
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import seaborn as sns
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import pdist
import pandas as pd

PREDICTOR = 'Conc /uM'
PREDICTOR = 'Drug'
SAVE_FIG = 1
FIG_PATH = './graphs/ML/'


plt.rcParams['figure.figsize'] = (14,10)

DRUGS = ['Control', 'Dabrafenib', 'G007', '5FU', 'Vemurafenib']
# DRUGS = ['Control', 'G007', '5FU', 'Vemurafenib']
DRUG = DRUGS[2]

predictions = {}

# csv_name = 'objects_image.csv'
# csv_name = '220519 - G007_objects_image.csv'
# csv_name = '190709_objects_image.csv'
csv_name =  './_analysed/230719/df_2Channel.csv'


df = pd.read_csv(csv_name,index_col=[0,1,2,3])

converter = {'Alive':0,
            'Dead': 1,
            'Unknown':2}
# converter['Alive']
live_dead_labels = df.index.to_frame()['Conc /uM'].apply(\
            lambda x: converter['Alive'] if x<0.1 else converter['Dead'] if x >=10 else converter['Unknown'])
df_and_livedead = df.copy()
df_and_livedead['Live/Dead'] =live_dead_labels
df_and_livedead =df_and_livedead.set_index('Live/Dead',append=True)
# df_and_livedead
# training = df_and_livedead.xs([0,1],level='Live/Dead')
idx = pd.IndexSlice

training = df_and_livedead.loc[idx[:,:,:,:,[0,1]],idx[:]]
testing = df_and_livedead.loc[idx[:,:,:,:,[2]],idx[:]]


training_values = StandardScaler().fit_transform(training)
training_labels = to_categorical(training.index.to_frame()['Live/Dead'].values,3)

testing_values = StandardScaler().fit_transform(testing)
testing_labels = to_categorical(testing.index.to_frame()['Live/Dead'].values,3)
# stesting_labels

model = Sequential()
# Dense(64) is a fully-connected layer with 64 hidden units.
# in the first layer, you must specify the expected input data shape:
# here, 20-dimensional vectors.
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(3, activation='softmax'))

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
# model.compile(loss='categorical_crossentropy',
#               optimizer='adam',
#               metrics=['accuracy'])

history=model.fit(training_values, training_labels,
          epochs=50,
          batch_size=128,
          shuffle=True,
          verbose=1)


# model.evaluate(x_train, y_train, batch_size=128)

predictions =np.argmax(model.predict(StandardScaler().fit_transform(df_and_livedead)),axis=1)
# predictions
df_predictions = df_and_livedead.copy()
df_predictions['Prediction'] = predictions
df_predictions = df_predictions.set_index('Prediction',append=True)
# df_predictions = df_predictions.drop('Prediction')

# df_livedead_predict = df_predictions.index.to_frame()[['Live/Dead','Prediction']]
# df_livedead_predict = df_predictions.index.to_frame().droplevel('ImageNumber')
# df_livedead_predict
# df_livedead_predict_er = df_livedead_predict.droplevel(['Live/Dead','Prediction'])
# df_no_imagenumber = df_livedead_predict_er.unstack(['Drug','Conc /uM']).groupby('ImageNumber').median().stack(['Conc /uM','Drug'])

# imagenumber_live
# df_predictions
# df_livedead_predict
df_livedead_predict = df_predictions.index.to_frame()[['Live/Dead','Prediction']]
index= df_livedead_predict.index
# df_livedead_predict.xs(2,level='Live/Dead').groupby('ImageNumber').count()
imagenumber_live = df_livedead_predict.xs(0,level='Live/Dead').groupby('ImageNumber').count()
imagenumber_dead = df_livedead_predict.xs(1,level='Live/Dead').groupby('ImageNumber').count()

imagenumber_live = imagenumber_live.\
        reindex(df_livedead_predict.index,level='ImageNumber').\
        drop_duplicates().\
        droplevel('ObjectNumber')
imagenumber_dead = imagenumber_dead.\
        reindex(df_livedead_predict.index,level='ImageNumber').\
        drop_duplicates().\
        droplevel('ObjectNumber')
imagenumber_dead = imagenumber_dead.rename(columns={'Prediction':'Dead'}).set_index('Live/Dead',append=True)
imagenumber_live = imagenumber_live.rename(columns={'Prediction':'Live'}).set_index('Live/Dead',append=True)
# imagenumber_dead
# imagenumber_live.join()
ok = imagenumber_live.merge(imagenumber_dead,on='ImageNumber',how='outer')
# ok.shape
good = ok.reindex(index,level='ImageNumber').drop_duplicates().droplevel('ObjectNumber')
good['Ratio']=good['Live']/good['Dead']
# sns.scatterplot(x='Conc /uM',y='Ratio',data=good.reset_index())
# imagenumber_dead
# imagenumber_dead = df_livedead_predict.xs(1,level='Live/Dead').groupby('ImageNumber').count()

# sns.bar(good)
working = good.unstack('Drug').groupby('Conc /uM').mean().stack()
working.plot(kind='bar',stacked=True)
good.plot(kind='bar',stacked=True)
good.reset_index()
# sns.barplot(hue='',good.reset_index());
# live_count =



df.unstack(['Drug','Conc /uM']).groupby('ImageNumber').count()
df_livedead_predict.xs(2,level='Live/Dead').groupby('ImageNumber',as_index=False).count()
# unknown_df
unknown_df=df_livedead_predict.xs(2,level='Live/Dead').unstack(['Drug','Conc /uM'])
df_livedead_predict
live_unknown_df = unknown_df.xs(0,level='Prediction')
live_unknown_df_count = live_unknown_df.groupby('ImageNumber').count()
live_unknown_df_count.stack(['Conc /uM','Drug'])

live_unknown_df_count
mess_with_me = live_unknown_df_count
mess_with_me
mess_with_me = mess_with_me.loc[~mess_with_me.index.duplicated(keep='first')]

# aa = mess_with_me.drop_duplicates(subset='rownum', keep='last')

dead_unknown_df = unknown_df.xs(1,level='Prediction')
dead_unknown_df_count = dead_unknown_df.groupby('ImageNumber').count()
dead_unknown_df_count.stack().stack()
live_unknown_df
dead_unknown_df_count

dead_unknown_df_count_stacked = dead_unknown_df_count.stack(['Conc /uM'])
dead_unknown_df_count_stacked_sum = dead_unknown_df_count_stacked.groupby('Conc /uM').mean()
data = dead_unknown_df_count_stacked_sum.stack('Drug').reset_index()
data
sns.relplot(x='Conc /uM',y='Prediction',col='Drug',data=data)

# df_livedead_predict.xs('G007',level='Drug')

dead_unknown_df_count


df_predictions['Live/Dead']
# df_predictions.iloc[:,:]  =predictions

# pd.DataFrame(df_predictions,index=df_predictions.index)

testing
