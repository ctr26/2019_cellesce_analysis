# ffmpeg -r 2 \
#         -f concat \
#         -safe 0 \
#         -i list.txt \
#         -c:v libx264 \
#         -pix_fmt yuv420p \
#         -vf "scale=2048:2048:force_original_aspect_ratio=decrease,pad=2048:2048:(ow-iw)/2:(oh-ih)/2"  out_yuv.mp4

ffmpeg -r 2 \
        -f concat \
        -safe 0 \
        -i list_images_best_plane_16_bit.txt \
        -c:v libx264 \
        -f avi \
        -vcodec rawvideo \
        -pix_fmt nv12 \
         -vf "scale=2048:2048:force_original_aspect_ratio=decrease,pad=2048:2048:(ow-iw)/2:(oh-ih)/2"  list_images_best_plane_16_bit.avi

# open out_nv12.avi
