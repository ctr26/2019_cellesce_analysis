docker run --rm -it \
      -v $(pwd):$(pwd) \
      linuxserver/ffmpeg \
      -f concat
      -safe 0
      -i list.txt
      -c:v libx264
      -r 25
      -pix_fmt yuv420p
      -t 15 out.mp4
