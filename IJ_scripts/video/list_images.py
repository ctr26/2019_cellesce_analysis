import os
import glob
import subprocess
file_names = ("best_plane",
              "best_plane_sobel")          
bit_names = ("16_bit",
             "32_bit")

file_names = ("best_plane_sobel",)
bit_names = ("16_bit", "32_bit")

for file_name in file_names:
    for bit_name in bit_names:
        dir = f"~/npl_ftp/**/*{file_name}_{bit_name}.png"
        dir = str(os.path.expanduser(dir))
        print(dir)
        # out = glob.glob("/homes/ctr26/npl_ftp/**/*best_plane_16_bit.png",recursive=True)
        # "/homes/ctr26/npl_ftp/**/*best_plane_sobel_32_bit.png"
        # "/homes/ctr26/npl_ftp/**/*best_plane_32_bit.png"        
        out = glob.glob(dir,recursive=True)
        print(out)
        file_list_out = [f'file \'{line}\'' for line in out]
        print(file_list_out)
        with open(f"list_images_{file_name}_{bit_name}.txt", "w") as outfile:
            outfile.write("\n".join(str(item) for item in file_list_out))
        # print(dir);
        # print(glob.glob(dir))

       

for file_name in file_names:
    for bit_name in bit_names:
        command = f"ffmpeg -y -r 2 \
        -f concat \
        -safe 0 \
        -i list_images_{file_name}_{bit_name}.txt \
        -c:v libx264 \
        -f avi \
        -vcodec rawvideo \
        -pix_fmt nv12 \
         -vf \"scale=2048:2048:force_original_aspect_ratio=decrease,pad=2048:2048:(ow-iw)/2:(oh-ih)/2\"  video_{file_name}_{bit_name}.avi"
        print(command)
        subprocess.call(command,shell=True)
