Folder: analysis

2d_cell_profiler_analysis_2_colour_celllines.py

Does the initial processing of combining all the output files from CellProfiler into a master table.
Once combined, it produces a summary barplot of all the conditions availiable in the dataset.
Data is filtered for columns that are useful and the dimensionality is reduced so as to contain 95% of the variance of the dataset
Does PCA to attempt to demonstrate clustering or lack thereof
Option for tSNEs, which are machine learning fuelled PCAs but very computationally expensive.
EC50s produced by scaling the dataset and taking the euclidian distance of each of the reduced and filtered vectors


machine_learning_conc_drug.py

Runs on the output of 2d_cell_profiler_analysis_2_colour_celllines.py
Takes the entire dataset sans one drug and Trains a NN to predict what drug the current observation has been exposed to.
The process is repeated for each drug to get a model for each drug.
The missing data is then run through the respective models to ask the question "Which drug does this drug objectivly look like"

Folder: IJ scripts

Helper scripts for dealing with MSquared's raw files.

Folder: 31072019_report

Mid-point report on the analysis pipline


